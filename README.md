# littleTiger
一款压缩图片软件
#### 描述
压缩图片的小软件，支持PNG，JPG，无损压缩，基于Tinypng开放API实现

#### 环境与框架
nodejs 14+、Vue.js 2.0、Electron

#### 初始化

1.项目根目录运行 npm install;
2.运行 npm dev

#### 打包
打包： npm build