import Vue from 'vue'
import Router from 'vue-router'
import homeView from '@/views/home'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: homeView
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
